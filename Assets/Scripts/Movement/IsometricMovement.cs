﻿using System;
using System.Collections;
using System.Collections.Generic;
using Assets.Scripts.Core;
using Assets.Scripts.Core.Events;
using Assets.Scripts.Core.Events.ConcreteEvents;
using UnityEngine;

public class IsometricMovement
    : ExtendedMonoBehaviour
{
    [SerializeField] private int _movementSpeed = 4;

    private Transform _cameraTransform;
    private bool _initialized;
    private bool _isMoving = false;

    private Vector3 _forward, _right = new Vector3();

    void Start()
    {
        if (Camera.main == null)
        {
            Logger.LogError("There is no MainCamera.");

            return;
        }

        _cameraTransform = Camera.main.transform;
        _forward = _cameraTransform.forward;
        _forward.y = 0;
        _forward = Vector3.Normalize(_forward);

        _right = Quaternion.Euler(new Vector3(0, 90, 0)) * _forward;

        _initialized = true;
    }

    void Update()
    {
        if(!_initialized)
            return;

        if (Input.anyKey)
            Move();
        else
        {
            if (_isMoving)
            {
                EventManager.Enque(GameEventManager.EventTypeId.PlayerMoves, new PlayerMoveEvent(false));
                _isMoving = false;
            }
        }
    }

    private void Move()
    {
        var horizontalAxis = Input.GetAxis("Horizontal");
        var verticalAxis = Input.GetAxis("Vertical");

        var horizontalMove = _right * _movementSpeed * Time.deltaTime * horizontalAxis;
        var verticalMove = _forward * _movementSpeed * Time.deltaTime * verticalAxis;

        var heading = Vector3.Normalize(horizontalMove + verticalMove);

        transform.forward = heading;
        transform.position += horizontalMove;
        transform.position += verticalMove;

        EventManager.Enque(GameEventManager.EventTypeId.PlayerMoves, new PlayerMoveEvent(true));
        _isMoving = true;
    }
}
