﻿using Assets.Scripts.Core;
using Assets.Scripts.Core.Events;
using Assets.Scripts.Core.Events.ConcreteEvents;
using UnityEngine;

namespace Assets.Scripts.AnimationControlls
{
    public class DoctorAnimationEventsListener
        : ExtendedMonoBehaviour
    {
        [SerializeField] private Animator _doctorAnimator;

        void Start()
        {
            if(_doctorAnimator == null)
                Logger.LogError($"Unassigned reference for { nameof(_doctorAnimator) }");

            EventManager.SubscribeTo(GameEventManager.EventTypeId.PlayerMoves, OnPlayerStateChanged);
        }

        private void OnPlayerStateChanged(GameEvent eventArgs)
        {
            var args = eventArgs as PlayerMoveEvent;

            if (args == null)
            {
                Logger.LogError("Cant cast event args to PlayerMoveEvent");
                return;
            }

            _doctorAnimator.SetBool("Running", args.IsPlayerMoving);
        }
    }
}