﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraFollow : MonoBehaviour
{
    [SerializeField] private Transform _target;
    [SerializeField] private Vector3 _offset;
    
    [Range(0.1f, 1f)]
    [SerializeField] private float _smoothSpeed = 0.125f;
    
    void Start()
    {
        _offset = transform.position - _target.transform.position;
    }

    void LateUpdate()
    {
        var newCameraPos = _target.transform.position + _offset;

        transform.position = Vector3.Slerp(transform.position, newCameraPos, _smoothSpeed);
    }
}
