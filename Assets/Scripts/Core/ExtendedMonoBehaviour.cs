﻿using Assets.Scripts.Core.Events;
using Assets.Scripts.Log.Concrete;
using UnityEngine;
using ILogger = Assets.Scripts.Log.Abstract.ILogger;

namespace Assets.Scripts.Core
{
    public class ExtendedMonoBehaviour : MonoBehaviour
    {
        public static ILogger Logger;
        public static GameEventManager EventManager;

        static ExtendedMonoBehaviour()
        {
            Logger = new UnityDebugLogger();
            EventManager = new GameEventManager();
        }
    }
}