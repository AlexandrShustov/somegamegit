﻿using System;
using System.Collections.Generic;

namespace Assets.Scripts.Core.Events
{
    public class GameEvent
    {

    }

    public class GameEventManager
    {
        public enum EventTypeId
        {
            PlayerMoves
        }

        private readonly Dictionary<EventTypeId, List<Action<GameEvent>>> _eventsPool;

        public GameEventManager()
        {
            _eventsPool = new Dictionary<EventTypeId, List<Action<GameEvent>>>();
        }

        public void SubscribeTo(EventTypeId eventType, Action<GameEvent> callback)
        {
            if (_eventsPool.ContainsKey(eventType))
            {
                _eventsPool[eventType].Add(callback);
            }
            else
            {
                _eventsPool.Add(eventType, new List<Action<GameEvent>>{ callback });
            }
        }

        public void Enque(EventTypeId eventType, GameEvent param)
        {
            if (_eventsPool.ContainsKey(eventType))
            {
                _eventsPool[eventType].ForEach(handler => handler.Invoke(param));
            }
        }

        public void Unsubscribe(EventTypeId eventType, Action<GameEvent> callback)
        {
            if (_eventsPool.ContainsKey(eventType))
                _eventsPool[eventType].Remove(callback);
        }
    }
}