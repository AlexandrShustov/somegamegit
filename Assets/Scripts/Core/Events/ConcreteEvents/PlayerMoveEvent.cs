﻿namespace Assets.Scripts.Core.Events.ConcreteEvents
{
    public class PlayerMoveEvent : GameEvent
    {
        public bool IsPlayerMoving;

        public PlayerMoveEvent(bool value)
        {
            IsPlayerMoving = value;
        }
    }
}