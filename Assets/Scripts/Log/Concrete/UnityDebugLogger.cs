﻿using Assets.Scripts.Log.Abstract;
using UnityEngine;
using ILogger = Assets.Scripts.Log.Abstract.ILogger;

namespace Assets.Scripts.Log.Concrete
{
    public class UnityDebugLogger : ILogger
    {
        public void LogWarning(string message)
        {
            Debug.LogWarning(message);
        }

        public void LogError(string message)
        {
            Debug.LogError(message);
        }
    }
}