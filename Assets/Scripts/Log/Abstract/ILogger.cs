﻿namespace Assets.Scripts.Log.Abstract
{
    public interface ILogger
    {
        void LogWarning(string message);
        void LogError(string message);
    }
}